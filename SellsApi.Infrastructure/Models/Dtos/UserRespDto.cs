using SellsApi.Infrastructure.Models.Entities;

namespace SellsApi.Infrastructure.Models.Dtos
{

    public class UserRespDto
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int RoleId { get; set; }

    }
}