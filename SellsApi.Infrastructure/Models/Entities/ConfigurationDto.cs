using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SellsApi.Infrastructure.Models.Entities
{
    public class ConfigurationDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Value { get; set; }
    }
}