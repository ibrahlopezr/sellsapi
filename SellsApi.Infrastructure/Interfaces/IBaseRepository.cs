using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SellsApi.Infrastructure.Models.Entities;

namespace SellsApi.Infrastructure.Interfaces
{

    public interface IBaseRepository<T> where T : BaseDto
    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<T> GetAsync(long id);
        Task InsertAsync(T entity);
        Task UpdateAsync(T entity);
        Task DeleteAsync(T entity);
    }

}