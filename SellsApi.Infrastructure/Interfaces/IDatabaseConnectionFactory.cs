using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SellsApi.Infrastructure.Interfaces
{
    public interface IDatabaseConnectionFactory
    {
        IDbConnection Connection { get; set; }
        void CreateConnectionAsync();
        void CloseConnectionAsync();

    }
}