﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SellsApi.Infrastructure.Interfaces.Context
{
    public interface IConnectionStringRepository
    {
        string getConnectionString();
    }
}
