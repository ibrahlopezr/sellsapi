using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SellsApi.Infrastructure.Models.Dtos;
using SellsApi.Infrastructure.Models.Entities;

namespace SellsApi.Infrastructure.Interfaces.Context
{
    public interface IUserRepository : IBaseRepository<UserDto>
    {
        /// <summary>
        /// Verify if exists user by email and username.
        /// </summary>
        /// <param name="Email">Email of user to register.</param>
        /// <param name="Username">Username from user to register.</param>
        /// <returns></returns>
        Task<bool> UserExist(string Email, string Username);
    }
}
