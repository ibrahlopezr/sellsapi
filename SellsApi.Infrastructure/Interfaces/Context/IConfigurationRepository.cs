﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SellsApi.Infrastructure.Models.Entities;

namespace SellsApi.Infrastructure.Interfaces.Context
{
    public interface IConfigurationRepository
    {
        Task<ConfigurationDto> GetConfiguration();
    }
}
