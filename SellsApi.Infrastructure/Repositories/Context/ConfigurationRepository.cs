﻿using Dapper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using SellsApi.Infrastructure.Models.Entities;
using SellsApi.Infrastructure.Helpers.Database;
using SellsApi.Infrastructure.Interfaces;
using SellsApi.Infrastructure.Interfaces.Context;

namespace SellsApi.Infrastructure.Repositories.Context
{
    public class ConfigurationRepository : IConfigurationRepository
    {
        /// <summary>
        /// Cadena de conexion.
        /// </summary>
        private readonly string ConnectionString;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="_connectionStringRepository"></param>
        public ConfigurationRepository(IConnectionStringRepository _connectionStringRepository)
        {
            ConnectionString = _connectionStringRepository.getConnectionString();
        }



        public async Task<ConfigurationDto> GetConfiguration()
        {
            try
            {
                ConfigurationDto data = null;
                using (IDbConnection con = new MySqlConnection(ConnectionString))
                {
                    con.Open();
                    data = await con.QueryFirstOrDefaultAsync<ConfigurationDto>(StoredProceduresWeb.GetConfiguration, commandType: CommandType.StoredProcedure);
                    con.Close();
                }

                return data;
            }
            catch (Exception ex)
            {

                throw new Exception($"Ocurrio un error al obtener la configuracion: {ex.Message}");
            }
        }
    }
}
