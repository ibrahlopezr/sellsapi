using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SellsApi.Infrastructure.Models.Entities;
using SellsApi.Infrastructure.Interfaces;
using SellsApi.Infrastructure.Interfaces.Context;
using SellsApi.Infrastructure.Models.Dtos;


namespace SellsApi.Infrastructure.Repositories.Context
{
    public class UserRepository : BaseRepository<UserDto>, IUserRepository
    {
        public UserRepository(IDatabaseConnectionFactory _database, BaseContext _context) : base(_database, _context)
        {
        }



        public async Task<bool> UserExist(string Email, string Username)
        {
            var response = (
                    await _.Users.Where(user => user.Email.Equals(Email) || user.Username.Equals(Username))
                    .Select(user =>
                    new UserRespDto()
                    {
                        Id = user.Id,
                        Email = user.Email,
                        Username = user.Username,
                        RoleId = user.RoleId,
                    })
                    .FirstOrDefaultAsync()
                ) is null;

            return !response;
        }

    }

}
