﻿using Microsoft.Extensions.Configuration;
using SellsApi.Infrastructure.Interfaces.Context;

namespace SellsApi.Infrastructure.Repositories.Context
{
    public class ConnectionStringRepository : IConnectionStringRepository
    {
        public string ConnectionString { get; }
        public ConnectionStringRepository(IConfiguration configuration)
        {
            ConnectionString = configuration.GetConnectionString("database");
        }

        public string getConnectionString() => ConnectionString;

    }
}
