﻿using System;
using System.Collections.Generic;
using System.Text;
using SellsApi.Infrastructure.Interfaces;
using SellsApi.Infrastructure.Interfaces.Context;
using SellsApi.Infrastructure.Repositories.Context;

namespace SellsApi.Infrastructure.Repositories.Context;

public interface IDataRepositories
{
    ConnectionStringRepository ConnectionStringRepository { get; set; }
    IConfigurationRepository ConfigurationRepository { get; set; }
    IUserRepository UserRepository { get; set; }
}

public class DataRepositories : IDataRepositories
{
    public ConnectionStringRepository ConnectionStringRepository { get; set; }

    public IConfigurationRepository ConfigurationRepository { get; set; }

    public IUserRepository UserRepository { get; set; }

    public DataRepositories(
        IConnectionStringRepository connectionStringRepository,
        IConfigurationRepository configurationRepository,
        IUserRepository userRepository
    )
    {
        ConnectionStringRepository = (ConnectionStringRepository)connectionStringRepository;
        ConfigurationRepository = (ConfigurationRepository)configurationRepository;
        UserRepository = (UserRepository)userRepository;
    }
}
