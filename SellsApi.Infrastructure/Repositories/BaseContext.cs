using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SellsApi.Infrastructure.Interfaces;
using SellsApi.Infrastructure.Models.Entities;

namespace SellsApi.Infrastructure.Repositories
{
    public class BaseContext : DbContext
    {
        public DbSet<UserDto> Users { get; set; }

        protected IDbConnection Connection { get; private set; }

        public BaseContext(
            DbContextOptions<BaseContext> options,
            IDatabaseConnectionFactory _database
        ) :
            base(options)
        {
            Connection = _database.Connection;
        }
    }
}
