using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using SellsApi.Infrastructure.Interfaces;
using Dapper;
using MySql.Data.MySqlClient;
using SellsApi.Infrastructure.Interfaces.Context;

namespace SellsApi.Infrastructure.Repositories
{
    public class SqlConnectionFactory : IDatabaseConnectionFactory
    {
        /// <summary>
        /// Connection string.
        /// </summary>
        private readonly string _connectionString;

        /// <summary>
        /// Connection.
        /// </summary>
        public IDbConnection Connection { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="connectionString">Database connection string</param>
        public SqlConnectionFactory(string connectionString)
        {
            _connectionString = connectionString ??
            throw new ArgumentException(nameof(connectionString));            
        }


        /// <summary>
        /// Open connection with database;
        /// </summary>
        /// <returns></returns>
        public void CreateConnectionAsync()
        {
            Connection = new MySqlConnection(_connectionString);
            Connection.Open();
            //return Connection;
        }

        /// <summary>
        /// Close connection with database.
        /// </summary>
        /// <returns></returns>
        public void CloseConnectionAsync()
        {
            Connection.Close();
            Connection.Dispose();
            Connection = null;
        }

        public async Task<IEnumerable<T>> QueryAsync<T>(string StoredProcedure, object parameters = null)
        {
            IEnumerable<T> result;
            try
            {
                CreateConnectionAsync();
                result = await Connection.QueryAsync<T>(StoredProcedure, parameters, commandType: CommandType.StoredProcedure);
                CloseConnectionAsync();

            }
            catch (Exception ex)
            {

                throw ex;
            }

            return result;
        }
        public async Task<T> QueryFirstOrDefaultAsync<T>(string StoredProcedure, object parameters = null)
        {
            T result;
            try
            {
                CreateConnectionAsync();
                result = await Connection.QueryFirstOrDefaultAsync<T>(StoredProcedure, parameters, commandType: CommandType.StoredProcedure);
                CloseConnectionAsync();

            }
            catch (Exception ex)
            {

                throw ex;
            }

            return result;
        }
    }
}
