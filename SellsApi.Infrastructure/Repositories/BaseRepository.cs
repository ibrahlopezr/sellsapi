using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SellsApi.Infrastructure.Interfaces;
using SellsApi.Infrastructure.Models.Entities;

namespace SellsApi.Infrastructure.Repositories
{
    public class BaseRepository<T> where T : BaseDto
    {
        protected IDbConnection Connection { get; private set; }
        public readonly BaseContext _;
        private IDatabaseConnectionFactory connection;
        private DbSet<T> entities;

        public BaseRepository(IDatabaseConnectionFactory _database, BaseContext _context)
        {
            Connection = _database.Connection;
            _ = _context;
            entities = _context.Set<T>();

        }

        public BaseRepository(IDatabaseConnectionFactory connection)
        {
            this.connection = connection;
        }


        #region Public Methods.

        /// <summary>
        /// Select data to entries from Entity Database.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            await Task.Yield();
            return entities.AsEnumerable();
        }

        /// <summary>
        /// Get data to entry database.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<T> GetAsync(long id)
        {
            return await entities.SingleOrDefaultAsync(s => s.Id == id);
        }

        /// <summary>
        /// Insert entry into database.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task InsertAsync(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            await entities.AddAsync(entity);
            await _.SaveChangesAsync();
        }

        /// <summary>
        /// Update entry from database.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task UpdateAsync(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            await _.SaveChangesAsync();
        }

        /// <summary>
        /// Delete Entry for database.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task DeleteAsync(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            entities.Remove(entity);
            await _.SaveChangesAsync();
        }
        #endregion


        #region Private Methods.
        private string GetTableName()
        {
            var dnAttribute = typeof(T).GetCustomAttributes(
                typeof(TableAttribute), true
            ).FirstOrDefault() as TableAttribute;
            if (dnAttribute != null)
            {
                return dnAttribute.Name;
            }
            return null;
        }

        /// <summary>
        /// Build content query update by datatype.
        /// </summary>
        /// <param name="obj">Object from Generic Class</param>
        /// <param name="prop">Propertie from Class.</param>
        /// <returns></returns>
        private static string buildQuery(T obj, PropertyInfo prop)
        {
            if (prop.PropertyType == typeof(string))
            {
                return $"{prop.Name} = '{prop.GetValue(obj, null)}'";
            }
            else if (prop.PropertyType == typeof(int))
            {
                return $"{prop.Name} = {prop.GetValue(obj, null)}";

            }
            else if (prop.PropertyType == typeof(bool))
            {
                return $"{prop.Name} = {prop.GetValue(obj, null)}";

            }
            else if (prop.PropertyType == typeof(DateTime))
            {

                return $"{prop.Name} = '{prop.GetValue(obj, null)}'";
            }
            else
            {
                return $"{prop.Name} = {prop.GetValue(obj, null)}";

            }
        }
        #endregion
    }
}