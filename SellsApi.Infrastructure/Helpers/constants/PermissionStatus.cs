namespace SellsApi.Infrastructure.Helpers.constants
{
    public enum PermissionStatus
    {
        CREATE,
        DELETE,
        UPDATE,
        SHOW
    }
}