using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SellsApi.Infrastructure.Helpers.Database
{
    public static class StoredProceduresWeb
    {


        #region Configurations
        public const string GetConfiguration = "WebApp_GetConfiguration";
        #endregion

        #region Members
        public const string GetMembers = "MobileApp_GetMembers";
        #endregion

    }
}