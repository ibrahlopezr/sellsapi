#See https://aka.ms/customizecontainer to learn how to customize your debug container and how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
USER app
WORKDIR /app
EXPOSE 8080
EXPOSE 8081

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
ARG BUILD_CONFIGURATION=Release
WORKDIR /src

COPY ["SellsApi/SellsApi.csproj", "SellsApi/"]
COPY ["SellsApi.Core/SellsApi.Core.csproj", "SellsApi.Core/"]
COPY ["SellsApi.Infrastructure/SellsApi.Infrastructure.csproj", "SellsApi.Infrastructure/"]

RUN dotnet restore "./SellsApi/./SellsApi.csproj"
COPY . .
WORKDIR "/src/SellsApi"
RUN dotnet build "./SellsApi.csproj" -c $BUILD_CONFIGURATION -o /app/build

FROM build AS publish
ARG BUILD_CONFIGURATION=Release
RUN dotnet publish "./SellsApi.csproj" -c $BUILD_CONFIGURATION -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "SellsApi.dll"]