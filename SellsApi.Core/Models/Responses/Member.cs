using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SellsApi.Core.Models.Responses
{
    public class Member
    {
        public int Id { get; set; }
        public string Fullname { get; set; }
        public int UserId { get; set; }
        public bool Active { get; set; }
    }
}