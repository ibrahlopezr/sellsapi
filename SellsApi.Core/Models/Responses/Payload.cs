using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SellsApi.Core.Models.Responses
{
    public class Payload
    {
        public string token { get; set; }
        public string[] routes { get; set; }
    }
}