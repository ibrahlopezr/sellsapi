﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SellsApi.Core.Models
{
    public class Response<T>
    {
        public string RequestId { get; set; }
        public T Data { get; set; }
        public string ErrorMsg { get; set; }
        // public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}
