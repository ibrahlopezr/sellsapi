﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SellsApi.Core.Models
{
    public enum Roles
    {
        NONE,
        ADMIN,
        TRAINER,
        MEMBER,
    }
}
