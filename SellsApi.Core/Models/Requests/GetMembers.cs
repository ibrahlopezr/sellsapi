using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SellsApi.Core.Models.Requests
{
    public class GetMembers
    {
        public int Paginate { get; set; }
    }
}