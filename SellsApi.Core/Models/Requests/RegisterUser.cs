using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SellsApi.Core.Models.Requests
{
    public class RegisterUser
    {
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}