using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SellsApi.Core.Models.Requests
{
    public class RefreshToken
    {
        public string Token { get; set; }
    }
}