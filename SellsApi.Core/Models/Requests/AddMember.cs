using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SellsApi.Core.Models.Requests
{
    public class AddMember
    {
        public string ConfirmPassword { get; set; }
        public string Email { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Password { get; set; }
        public string SecondLastname { get; set; }
        public string Secondname { get; set; }
        public string Username { get; set; }

    }
}