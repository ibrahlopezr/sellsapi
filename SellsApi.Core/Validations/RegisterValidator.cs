﻿using FluentValidation;
using req = SellsApi.Core.Models.Requests;

namespace SellsApi.Core.Validations
{
    public class RegisterValidator : AbstractValidator<req.RegisterUser>
    {
        public RegisterValidator()
        {
            RuleFor(x => x.Email).NotNull().EmailAddress();
            RuleFor(x => x.Password).NotNull().Equal(a => a.ConfirmPassword).WithMessage("Passwords do not match.").Length(8);
            RuleFor(x => x.Username).NotNull();
            RuleFor(x => x.ConfirmPassword).NotNull();
        }
    }
}
