using AutoMapper;
using SellsApi.Core.Helpers.Exceptions.Core;
using SellsApi.Core.Models;
using SellsApi.Core.Models.Responses;
using SellsApi.Infrastructure.Models.Dtos;

namespace SellsApi.Core.Mapping.Resolvers
{
    public class UserRolNameResolvers : IMemberValueResolver<UserRespDto, User, int, string>
    {

        public string Resolve(UserRespDto source, User destination, int sourceMember, string destMember, ResolutionContext context)
        {
            return source.RoleId switch
            {
                ((int)Roles.ADMIN) => nameof(Roles.ADMIN),
                ((int)Roles.TRAINER) => nameof(Roles.TRAINER),
                ((int)Roles.MEMBER) => nameof(Roles.MEMBER),
                ((int)Roles.NONE) => nameof(Roles.NONE),
                _ => throw new RoleNotDefinedException($"Role {source.RoleId} isn't defined")
            };
        }
    }
}