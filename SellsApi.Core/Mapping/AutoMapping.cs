﻿using System;
using AutoMapper;
using SellsApi.Core.Helpers.Exceptions;
using SellsApi.Core.Helpers.Security;
using SellsApi.Core.Mapping.Resolvers;

using dtos = SellsApi.Infrastructure.Models.Dtos;
using ent = SellsApi.Infrastructure.Models.Entities;
using req = SellsApi.Core.Models.Requests;
using res = SellsApi.Core.Models.Responses;

namespace SellsApi.Core.Mapping
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            try
            {

#region Custom Mapping
                CreateMap<dtos.UserRespDto, res.User>()
                    .ForMember(dest => dest.Id,
                    source => source.MapFrom(a => a.Id))
                    .ForMember(dest => dest.Email,
                    source => source.MapFrom(a => a.Email))
                    .ForMember(dest => dest.Username,
                    source => source.MapFrom(a => a.Username))
                    .ForMember(dest => dest.Password,
                    source => source.MapFrom(a => a.Password.Decrypt()))
                    .ForMember(dest => dest.RoleId,
                    source => source.MapFrom(a => a.RoleId))
                    .ForMember(dest => dest.RoleName,
                    source =>
                        source
                            .MapFrom<UserRolNameResolvers, int>(a => a.RoleId))
                    .ReverseMap()
                    .ForPath(dest => dest.Id,
                    source => source.MapFrom(a => a.Id))
                    .ForPath(dest => dest.Email,
                    source => source.MapFrom(a => a.Email))
                    .ForPath(dest => dest.Username,
                    source => source.MapFrom(a => a.Username))
                    .ForPath(dest => dest.Password,
                    source => source.MapFrom(a => a.Password.Decrypt()))
                    .ForPath(dest => dest.RoleId,
                    source => source.MapFrom(a => a.RoleId));

                CreateMap<req.RegisterUser, ent.UserDto>()
                    .ForMember(dest => dest.Email,
                    opt => opt.MapFrom(a => a.Email))
                    .ForMember(dest => dest.Username,
                    opt => opt.MapFrom(a => a.Username))
                    .ForMember(dest => dest.Password,
                    opt => opt.MapFrom(a => a.Password.Encrypt()));

                CreateMap<req.Login, ent.UserDto>()
                    .ForMember(dest => dest.Username,
                    opt => opt.MapFrom(a => a.Username))
                    .ForMember(dest => dest.Password,
                    opt => opt.MapFrom(a => a.Password.Encrypt()));
            

                CreateMap<req.AddMember, req.RegisterUser>()
                    .ForMember(dest => dest.Email,
                    opt => opt.MapFrom(a => a.Email))
                    .ForMember(dest => dest.Username,
                    opt => opt.MapFrom(a => a.Username))
                    .ForMember(dest => dest.Password,
                    opt => opt.MapFrom(a => a.Password))
                    .ForMember(dest => dest.ConfirmPassword,
                    opt => opt.MapFrom(a => a.ConfirmPassword));
#endregion

            }
            catch (BaseException baseex)
            {
                throw baseex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
