﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Reflection;
using SellsApi.Core.Mapping;
using SellsApi.Core.Services;
using SellsApi.Core.Services.Interfaces;
using SellsApi.Infrastructure.Interfaces.Context;
using SellsApi.Infrastructure.Repositories.Context;
using SellsApi.Infrastructure.Repositories;
using SellsApi.Infrastructure.Interfaces;
using FluentValidation;
using SellsApi.Core.Models.Requests;
using SellsApi.Core.Validations;

namespace SellsApi.Core.DependencyInjection
{
    public static class ServiceExtensions
    {

        /// <summary>
        /// Registro de repositorios.
        /// </summary>
        /// <param name="repositories"></param>
        /// <returns></returns>
        public static IServiceCollection RegisterRepositories(this IServiceCollection repositories, IConfiguration configuration)
        {

            repositories.TryAddTransient<IDatabaseConnectionFactory>(e => new SqlConnectionFactory(configuration.GetConnectionString("database")));
            repositories.TryAddScoped<IConnectionStringRepository, ConnectionStringRepository>();
            repositories.TryAddScoped<IConfigurationRepository, ConfigurationRepository>();
            repositories.TryAddScoped<IUserRepository, UserRepository>();


            return repositories;
        }

        /// <summary>
        /// Registro de Servicios.
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection RegisterServices(
            this IServiceCollection services)
        {

            services.TryAddTransient<IAuthenticationService, AuthenticationService>();
            services.TryAddTransient<IJwtService, JwtService>();
            services.TryAddTransient<IUserService, UserService>();

            return services;
        }


        /// <summary>
        /// Registro de DataServices
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection RegisterDataServices(
            this IServiceCollection services)
        {
            services.TryAddTransient<IServiceFactory, ServiceFactory>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            // Add all other services here.
            return services;
        }

        public static IServiceCollection RegisterValidationsServices(this IServiceCollection services)
        {
            services.AddScoped<IValidator<RegisterUser>, RegisterValidator>();
            return services;
        }


        /// <summary>
        /// Registro de DAtaRepositories.
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection RegisterDataRepositories(
            this IServiceCollection services)
        {
            services.TryAddTransient<IDataRepositories, DataRepositories>();

            // Add all other services here.
            return services;
        }

        /// <summary>
        /// Agregar dependencias a AutoMapper.
        /// </summary>
        /// <param name="services"></param>
        /// <param name="assemblies"></param>
        public static void AddDependenciesMyLibrary(this IServiceCollection services, IList<Assembly> assemblies)
        {
            // add automapper
            assemblies.Add(Assembly.GetAssembly(typeof(AutoMapping)));
            // add registration of DI libraries
        }

    }
}
