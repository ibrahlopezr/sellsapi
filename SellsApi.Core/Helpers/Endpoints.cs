using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SellsApi.Core.Helpers
{
    public static class Endpoints
    {
        #region MemberController
        public const string GetMembers = "all";
        public const string AddMember = "add-member";
        #endregion

        #region UsersController
        public const string GetUsers = "users";
        #endregion

        #region AuthController
        public const string Login = "login";
        public const string Register = "register";
        public const string ChangePassword = "change-password";
        public const string ForgotPassword = "forgot-password";
        public const string RefreshToken = "refresh-token";
        #endregion

    }
}