using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SellsApi.Core.Helpers.Exceptions.Core
{
    public class UserExistsException : BaseException
    {
        public UserExistsException()
        {
        }

        public UserExistsException(string message) :
            base(message)
        {
        }

        public UserExistsException(string message, Exception inner) :
            base(message, inner)
        {
        }
    }
}