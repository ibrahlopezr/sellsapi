using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SellsApi.Core.Helpers.Exceptions.Core
{
    public class NotFoundEntryException : BaseException
    {
        public NotFoundEntryException()
        {
        }

        public NotFoundEntryException(string message) :
            base(message)
        {
        }

        public NotFoundEntryException(string message, Exception inner) :
            base(message, inner)
        {
        }
    }
}
