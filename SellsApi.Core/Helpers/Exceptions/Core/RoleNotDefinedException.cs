using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SellsApi.Core.Helpers.Exceptions.Core
{
    public class RoleNotDefinedException : BaseException
    {
        public RoleNotDefinedException()
        {
        }

        public RoleNotDefinedException(string message) : base(message)
        {
        }

        public RoleNotDefinedException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}