﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SellsApi.Core.Helpers
{
    public class ApplicationSettings
    {
        public string JWT_Secret { get; set; }
        public string ExpirationInMinutes { get; set; }
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public int RefreshTokenValidityInDays { get; set; }
    }
}
