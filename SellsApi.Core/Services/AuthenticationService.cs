﻿using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using SellsApi.Core.Helpers.Exceptions.Core;
using SellsApi.Core.Models;
using SellsApi.Core.Services.Interfaces;
using SellsApi.Infrastructure.Repositories.Context;

using Dto = SellsApi.Infrastructure.Models.Dtos;
using ent = SellsApi.Infrastructure.Models.Entities;
using req = SellsApi.Core.Models.Requests;
using res = SellsApi.Core.Models.Responses;

namespace SellsApi.Core.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        private readonly IDataRepositories _repositoryFactory;

        private readonly IJwtService _jwtService;

        private readonly IMapper _mapper;

        public AuthenticationService(
            IHttpContextAccessor httpContextAccessor,
            IDataRepositories repositoryFactory,
            IMapper mapper,
            IJwtService jwtService
        )
        {
            _httpContextAccessor = httpContextAccessor;
            _repositoryFactory = repositoryFactory;
            _mapper = mapper;
            _jwtService = jwtService;
        }

        public async Task<int> GetUserId()
        {
            var claimsIdentity =
                _httpContextAccessor.HttpContext.User.Identity as
                ClaimsIdentity;
            var userId = claimsIdentity.FindFirst("id")?.Value;
            return await Task.FromResult(int.Parse(userId));
        }

        public async Task<int> GetUserIdCliente()
        {
            var claimsIdentity =
                _httpContextAccessor.HttpContext.User.Identity as
                ClaimsIdentity;
            var userId = claimsIdentity.FindFirst("idCliente")?.Value;
            return await Task.FromResult(int.Parse(userId));
        }

        public async Task<string> GetUserRole()
        {
            var claimsIdentity =
                _httpContextAccessor.HttpContext.User.Identity as
                ClaimsIdentity;
            var userRole = claimsIdentity.FindFirst(ClaimTypes.Role)?.Value;
            return await Task.FromResult(userRole);
        }

        public async Task<string> GetUsername()
        {
            var claimsIdentity =
                _httpContextAccessor.HttpContext.User.Identity as
                ClaimsIdentity;
            var UserName = claimsIdentity.FindFirst("username")?.Value;
            return await Task.FromResult(UserName);
        }

        public async Task<int> Register(req.RegisterUser request)
        {
            bool exist =
                await _repositoryFactory
                    .UserRepository
                    .UserExist(Email: request.Email,
                    Username: request.Username);
            if (exist)
                throw new UserExistsException("Email or username already exists");
            var model = _mapper.Map<ent.UserDto>(request);
            model.RoleId = (int)Roles.MEMBER;
            await _repositoryFactory.UserRepository.InsertAsync(model);
            return model.Id;
        }

        public async Task<res.Payload> Login(req.Login request)
        {
            res.Payload result = new res.Payload();
            var model = _mapper.Map<ent.UserDto>(request);
            var response = "";
            if (response == null)
                throw new UserExistsException("User or password is wrong.");
            var user = _mapper.Map<res.User>(response);
            result.token = _jwtService.GenerateSecurityToken(user);

            return await Task.FromResult(result);
        }
    }
}
