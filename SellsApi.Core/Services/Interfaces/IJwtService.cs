﻿using System;
using System.Collections.Generic;
using System.Text;
using res = SellsApi.Core.Models.Responses;

namespace SellsApi.Core.Services.Interfaces
{
    public interface IJwtService
    {
        string GenerateSecurityToken(res.User user);
    }
}
