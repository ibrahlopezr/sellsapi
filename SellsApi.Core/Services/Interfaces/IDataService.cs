using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SellsApi.Core.Services.Interfaces
{
    public interface IServiceFactory
    {
        IUserService UserService { get; set; }

        IAuthenticationService AuthService { get; set; }

        IJwtService JwtService { get; set; }
    }
}
