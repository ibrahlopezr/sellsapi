﻿using System.Threading.Tasks;
using req = SellsApi.Core.Models.Requests;
using res = SellsApi.Core.Models.Responses;

namespace SellsApi.Core.Services.Interfaces
{
    /// <summary>
    /// Interface para el manejo de la autenticacion.
    /// </summary>
    public interface IAuthenticationService
    {
        /// <summary>
        /// Obtener el Id del usuario loggueado.
        /// </summary>
        /// <returns></returns>
        Task<int> GetUserId();

        /// <summary>
        /// Obtener IdCliente del usuario loggueado.
        /// </summary>
        /// <returns></returns>
        Task<int> GetUserIdCliente();

        /// <summary>
        /// Obtener el role del usuario loggueado.
        /// </summary>
        /// <returns></returns>
        Task<string> GetUserRole();

        /// <summary>
        /// Obtener el nombre de usuario.
        /// </summary>
        /// <returns></returns>
        Task<string> GetUsername();

        /// <summary>
        /// Register user in database with password encrypted.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<int> Register(req.RegisterUser request);
        /// <summary>
        /// Method to login user and generate token.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<res.Payload> Login(req.Login request);
    }
}
