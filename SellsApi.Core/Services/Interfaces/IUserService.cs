using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using res = SellsApi.Core.Models.Responses;
namespace SellsApi.Core.Services.Interfaces
{
    public interface IUserService
    {
        /// <summary>
        /// Return all users.
        /// </summary>
        /// <returns></returns>
        Task<List<res.User>> GetUsers();
        /// <summary>
        /// Find user by id.
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<res.User> GetUserById(int Id);
    }
}