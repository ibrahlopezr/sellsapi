using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using SellsApi.Core.Models.Responses;
using SellsApi.Core.Services.Interfaces;
using SellsApi.Infrastructure.Repositories.Context;

namespace SellsApi.Core.Services
{
    public class UserService : IUserService
    {
        private readonly IDataRepositories RepositoriesFactory;
        private readonly IMapper Mapper;
        public UserService(IDataRepositories _repositoriesFactory, IMapper _mapper)
        {
            RepositoriesFactory = _repositoriesFactory;
            Mapper = _mapper;
        }
        public async Task<List<User>> GetUsers()
        {
            var data = await RepositoriesFactory.UserRepository.GetAllAsync();
            var response = Mapper.Map<List<User>>(data);
            return response;
        }

        public async Task<User> GetUserById(int Id)
        {
            var data = await RepositoriesFactory.UserRepository.GetAsync(Id);
            var response = Mapper.Map<User>(data);
            return response;
        }
    }
}