using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SellsApi.Core.Services.Interfaces;

namespace SellsApi.Core.Services
{
    public class ServiceFactory : IServiceFactory
    {

        public IUserService UserService { get; set; }
        public IAuthenticationService AuthService { get; set; }
        public IJwtService JwtService { get; set; }

        public ServiceFactory(IUserService _userService, IAuthenticationService _authService, IJwtService _jwtService)
        {
            UserService = _userService;
            AuthService = _authService;
            JwtService = _jwtService;
        }



    }
}