﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using res = SellsApi.Core.Models.Responses;
using SellsApi.Core.Helpers;
using SellsApi.Core.Services.Interfaces;

namespace SellsApi.Core.Services
{
    public class JwtService : IJwtService
    {
        private readonly ApplicationSettings _appSettings;
        public JwtService(IOptions<ApplicationSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public string GenerateSecurityToken(res.User model)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.JWT_Secret);


            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim("id", model.Id.ToString()),
                    new Claim("name", model.Name),
                    new Claim("lastname", model.Lastname),
                    new Claim("secondLastname", model.SecondLastname),
                    new Claim(JwtRegisteredClaimNames.Sub, model.Username),
                    new Claim(ClaimTypes.Role, model.RoleId.ToString()),
                    new Claim("roleName", model.RoleName),
                    new Claim(JwtRegisteredClaimNames.Email, model.Email),
                    new Claim(JwtRegisteredClaimNames.UniqueName, model.Username),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                }),
                Audience = _appSettings.Audience,
                Issuer = _appSettings.Issuer,
                Expires = DateTime.UtcNow.AddMinutes(double.Parse(_appSettings.ExpirationInMinutes)),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var SecurityToken = tokenHandler.CreateToken(tokenDescriptor);
            var Token = tokenHandler.WriteToken(SecurityToken);
            return Token;

        }
    }
}
