using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using SellsApi.Infrastructure.Repositories;
using SellsApi.Swagger;
using SellsApi.Core.Helpers;
using SellsApi.Extensions.Services;
using SellsApi.Core.Middlewares;
using SellsApi.Extensions.Middlewares;

namespace SellsApi
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }


        public void ConfigureServices(IServiceCollection services)
        {
            services.AddServiceMapper();

            services.AddDbContext<BaseContext>(options => options.UseMySQL(Configuration.GetSection("ConnectionStrings:database").Value));

            services.AddServiceCors();

            services.Configure<ApplicationSettings>(Configuration.GetSection("ApplicationSettings"));

            services.AddDependencyInjection(Configuration);

            services.AddTokenAuthentication(Configuration);

            services.AddServiceController();

            services.Configure<ApiBehaviorOptions>(options => options.SuppressModelStateInvalidFilter = true);

            services.AddServiceSwagger();

            CultureInfoExtension.AddCultureInfo();

        }



        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            #region Swaggers

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("v1/swagger.json", "templateFactory API V1");
            });

            #endregion

            #region Cors
            app.UseCors(PolicyCors._AllowOrigin);
            #endregion

            #region Middlewares
            app.UseMiddleware<JwtMiddleware>();
            #endregion
            app.UseHttpsRedirection();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}