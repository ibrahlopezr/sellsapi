﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SellsApi.Core.Services.Interfaces;
using Microsoft.AspNetCore.Cors;
using SellsApi.Core.Helpers;
using Microsoft.AspNetCore.Http;
using req = SellsApi.Core.Models.Requests;
using res = SellsApi.Core.Models.Responses;
using SellsApi.Core.Models;
using Swashbuckle.AspNetCore.Annotations;
using SellsApi.Core.Helpers.Exceptions;
using System.Collections.Generic;

namespace SellsApi.Controllers
{
    [Route("api/auth", Name = "Authentication Controller")]
    [Produces("application/json")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IServiceFactory _serviceFactory;

        public AuthController(IServiceFactory serviceFactory)
        {
            _serviceFactory = serviceFactory;
        }
        [HttpPost(Endpoints.Login)]
        [SwaggerOperation("Method to login.")]
        [ProducesResponseType(typeof(res.Payload), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(Response<List<String>>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(Response<bool>), StatusCodes.Status409Conflict)]
        [ProducesResponseType(typeof(Response<dynamic>), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Login([FromBody] req.Login request)
        {

            ActionResult result = null;
            Response<dynamic> response = new Response<dynamic>();

            try
            {
                var data = await _serviceFactory.AuthService.Login(request);
                result = Ok(data);
            }
            catch (BaseException ex)
            {
                response.ErrorMsg = ex.Message;
                result = StatusCode(StatusCodes.Status409Conflict, response);
            }
            catch (System.Exception ex)
            {
                response.ErrorMsg = ex.Message;
                result = StatusCode(StatusCodes.Status500InternalServerError, response);
            }

            return result;
        }

        [HttpPost(Endpoints.Register)]
        [SwaggerOperation("Method to register user.")]
        [ProducesResponseType(typeof(Response<int>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(Response<List<String>>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(Response<bool>), StatusCodes.Status409Conflict)]
        [ProducesResponseType(typeof(Response<dynamic>), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Register([FromBody] req.RegisterUser request)
        {
            ActionResult result = null;
            Response<int> response = new Response<int>();
            try
            {
                response.Data = await _serviceFactory.AuthService.Register(request);
                result = Ok(response);
            }
            catch (BaseException ex)
            {
                response.ErrorMsg = ex.Message;
                result = StatusCode(StatusCodes.Status409Conflict, response);
            }
            catch (System.Exception ex)
            {
                response.ErrorMsg = ex.Message;
                result = StatusCode(StatusCodes.Status500InternalServerError, response);
            }
            return result;
        }

        [HttpPost(Endpoints.RefreshToken)]
        public async Task<IActionResult> RefreshToken([FromBody] dynamic request)
        {
            ActionResult result = null;
            Response<dynamic> response = new Response<dynamic>();
            try
            {
                var data = await Task.FromResult(_serviceFactory.GetType());
                result = Ok("new-token");
            }
            catch (BaseException ex)
            {
                response.ErrorMsg = ex.Message;
                result = StatusCode(StatusCodes.Status400BadRequest, response);

            }
            catch (Exception ex)
            {

                response.ErrorMsg = ex.Message;
                result = StatusCode(StatusCodes.Status500InternalServerError, response);
            }

            return result;
        }




        [HttpPost(Endpoints.ChangePassword)]
        [SwaggerOperation("Change password for user.")]
        public async Task<IActionResult> ChangePassword([FromBody] dynamic model)
        {
            ActionResult response = null;

            try
            {
                var data = await Task.FromResult(_serviceFactory.GetType());
                response = Ok(response);
            }
            catch (System.Exception ex)
            {

                response = StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
            return response;
        }

        [HttpPost(Endpoints.ForgotPassword)]
        [SwaggerOperation("Forgot password")]
        public async Task<IActionResult> ForgotPassword([FromBody] dynamic model)
        {
            ActionResult response = null;

            try
            {
                var data = await Task.FromResult(_serviceFactory.GetType());
                response = Ok(response);
            }
            catch (System.Exception ex)
            {

                response = StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
            return response;
        }
    }
}



