using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using SellsApi.Core.Helpers;
using SellsApi.Core.Models;
using SellsApi.Core.Services.Interfaces;

namespace SellsApi.Controllers
{
    [ApiController]
    [Route("api/users", Name = "Users Controller")]
    public class UsersController : ControllerBase
    {
        /// <summary>
        /// Service factory.
        /// </summary>
        private readonly IServiceFactory serviceFactory;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="_serviceFactory"></param>
        public UsersController(IServiceFactory _serviceFactory)
        {
            serviceFactory = _serviceFactory;
        }



        [HttpGet(Endpoints.GetUsers)]
        [SwaggerOperation("Get users list.")]
        public async Task<ActionResult> Get()
        {
            ActionResult result = null;
            Response<dynamic> response = new Response<dynamic>();
            try
            {
                response.Data = await serviceFactory.UserService.GetUsers();
                result = Ok(response);
            }
            catch (Exception ex)
            {
                response.ErrorMsg = ex.Message;
                result = StatusCode(StatusCodes.Status500InternalServerError, response);
            }

            return result;
        }


    }
}