using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Swashbuckle.AspNetCore.Filters;
using res = SellsApi.Core.Models.Requests;
namespace SellsApi.Swagger.AuthController.Requests
{
    public class Login : IExamplesProvider<res.Login>
    {
        public res.Login GetExamples()
        {
            return new res.Login()
            {
                Username = "ibrahlopez",
                Password = "12345678"
            };
        }
    }
}