
using Swashbuckle.AspNetCore.Filters;
using res = SellsApi.Core.Models.Requests;
namespace Namespace
{
    public class RegisterUser : IExamplesProvider<res.RegisterUser>
    {
        public res.RegisterUser GetExamples()
        {
            return new res.RegisterUser(){
                Email = "ibrahlopezr@gmail.com",
                Username = "ibrahimlopez",
                Password = "12345678",
                ConfirmPassword = "12345678"
            };
        }
    }
}