using Swashbuckle.AspNetCore.Filters;
using SellsApi.Core.Models.Requests;
using req = SellsApi.Core.Models.Requests;
namespace SellsApi.Swagger.Requests.MembersController
{
    public class GetMember : IExamplesProvider<req.GetMembers>
    {
        public GetMembers GetExamples()
        {
            return new GetMembers
            {
                Paginate = 5
            };
        }
    }
}