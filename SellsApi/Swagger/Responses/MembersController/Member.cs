using res = SellsApi.Core.Models.Responses;
using Swashbuckle.AspNetCore.Filters;
using System.Collections.Generic;

namespace SellsApi.Swagger.Responses.MembersController
{
    public class MemberList : IExamplesProvider<List<res.Member>>
    {
        public List<res.Member> GetExamples()
        {
            return new List<res.Member>(){
                new res.Member()
                {
                    Id = 1,
                    Fullname = "Ibrahim Alexis Lopez Roman",
                    UserId = 1,
                    Active = true
                }
            };
        }
    }
    public class Member : IExamplesProvider<res.Member>
    {
        public res.Member GetExamples()
        {

            return new res.Member()
            {
                Id = 1,
                Fullname = "Ibrahim Alexis Lopez Roman",
                UserId = 1,
                Active = true
            };

        }
    }
}