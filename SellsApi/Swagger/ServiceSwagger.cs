using System;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Filters;

namespace SellsApi.Swagger
{
    public static class ServiceSwagger
    {
        public static IServiceCollection AddServiceSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(opt =>
          {
              var groupName = "v1";

              opt.SwaggerDoc(groupName, new OpenApiInfo
              {
                  Title = $"Template Factory API {groupName}",
                  Version = groupName,
                  Description = string.Empty,
                  Contact = new OpenApiContact
                  {
                      Name = "templateFactory API",
                      Email = string.Empty,
                      Url = new Uri("https://example.com.mx/"),
                  }
              });

              // Include 'SecurityScheme' to use JWT Authentication
              var jwtSecurityScheme = new OpenApiSecurityScheme
              {
                  Scheme = "bearer",
                  BearerFormat = "JWT",
                  Name = "JWT Authentication",
                  In = ParameterLocation.Header,
                  Type = SecuritySchemeType.Http,
                  Description = "Put **_ONLY_** your JWT Bearer token on textbox below!",

                  Reference = new OpenApiReference
                  {
                      Id = JwtBearerDefaults.AuthenticationScheme,
                      Type = ReferenceType.SecurityScheme
                  }
              };

              opt.AddSecurityDefinition(jwtSecurityScheme.Reference.Id, jwtSecurityScheme);

              opt.AddSecurityRequirement(
                new OpenApiSecurityRequirement
                {
                    { jwtSecurityScheme, Array.Empty<string>() }
              });
              
              opt.ExampleFilters();

          });
            services.AddSwaggerExamplesFromAssemblyOf<Startup>();
            return services;
        }
    }
}