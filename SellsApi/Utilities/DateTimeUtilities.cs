﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace SellsApi.Utilities
{
    public static class DateTimeUtilities
    {

        /// <summary>
        /// Funcion para obtener un listado de todos los meses.;
        /// </summary>
        /// <returns>Todos los meses</returns>
        public static List<string> getAllMonths()
        {
            List<string> objMeses = DateTimeFormatInfo.CurrentInfo.MonthNames.ToList();
            objMeses.Remove(objMeses.LastOrDefault());
            return objMeses;
        }

        /// <summary>
        /// Obtener los ultimos 3 años a la fecha actual.
        /// </summary>
        /// <returns>Lista de años</returns>
        public static IEnumerable<int> getLastThreeYears() => Enumerable.Range(1, 3).Select(i => DateTime.Now.AddYears(i - 3)).Select(date => int.Parse(date.ToString("yyyy")));
        public static IEnumerable<int> getNextThreeYears() => Enumerable.Range(1, 3).Select(i => DateTime.Now.AddYears(i - 1)).Select(date => int.Parse(date.ToString("yyyy")));
    }
}
