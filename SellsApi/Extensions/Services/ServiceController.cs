using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation.AspNetCore;
using Microsoft.Extensions.DependencyInjection;
using SellsApi.Core.Middlewares.Validators;

namespace SellsApi.Extensions.Services
{
    public static class ServiceController
    {
        public static IServiceCollection AddServiceController(this IServiceCollection services)
        {
            services
            .AddControllers(options => options.Filters.Add(typeof(ValidateModelStateAttribute)))
            .AddFluentValidation()
            .AddJsonOptions(opts => opts.JsonSerializerOptions.PropertyNamingPolicy = null)
            .AddNewtonsoftJson(x =>
                x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            );
            return services;
        }
    }
}