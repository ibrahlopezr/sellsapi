using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using System.Globalization;

namespace SellsApi.Extensions.Services
{
    public static class CultureInfoExtension
    {
        public static void AddCultureInfo()
        {
            //Culture info
            var cultureInfo = new CultureInfo("es-MX");
            //cultureInfo.NumberFormat.CurrencySymbol = "�";

            CultureInfo.DefaultThreadCurrentCulture = cultureInfo;
            CultureInfo.DefaultThreadCurrentUICulture = cultureInfo;
        }
    }
}