using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using SellsApi.Core.DependencyInjection;

namespace SellsApi.Extensions.Services
{
    public static class ServiceMapper
    {
        public static IServiceCollection AddServiceMapper(this IServiceCollection services)
        {
            var assemblies = new List<Assembly>();
            // register DI and add to the assemblies collection
            services.AddDependenciesMyLibrary(assemblies);
            // add the apps local automapper config 
            assemblies.Add(Assembly.GetAssembly(typeof(Startup)));
            // Initialise AutoMapper
            services.AddAutoMapper(assemblies);
            return services;
        }
    }
}