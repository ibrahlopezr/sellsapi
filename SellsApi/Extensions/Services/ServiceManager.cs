using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SellsApi.Core.DependencyInjection;

namespace SellsApi.Extensions.Services
{
    public static class ServiceManager
    {
        public static IServiceCollection AddDependencyInjection(this IServiceCollection services, IConfiguration Configuration)
        {
            services.RegisterRepositories(Configuration);
            services.RegisterServices();
            services.RegisterDataRepositories();
            services.RegisterDataServices();
            services.RegisterValidationsServices();
            return services;
        }
    }
}