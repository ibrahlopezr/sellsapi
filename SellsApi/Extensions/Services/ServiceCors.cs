using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using SellsApi.Core.Helpers;

namespace SellsApi.Extensions.Services
{
    public static class ServiceCors
    {
        public static IServiceCollection AddServiceCors(this IServiceCollection services)
        {
            services.AddCors(c =>
            {
                c.AddPolicy(PolicyCors._AllowOrigin, options =>
                {
                    options.AllowAnyHeader();
                    options.AllowAnyOrigin();
                    options.AllowAnyMethod();
                });
            });
            return services;
        }
    }
}